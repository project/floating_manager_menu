jQuery(document).ready(function() {
  jQuery("#floating-manager-menu").draggable({
    handle: 'h2',
    opacity: 0.4,
    stop: function() {
      jQuery.get(Drupal.settings.basePath + 'floatingmanagermenu/positioning_set/' + jQuery('#floating-manager-menu').css('top') + '/' + jQuery('#floating-manager-menu').css('left') + '/' + jQuery('#floating-manager-menu .inner').css('display'));
    }
  }).disableSelection();
});

function togglefmm() {
  jQuery("#floating-manager-menu").toggleClass("collapsed");
  var blockstyle = '';
  if(jQuery('#floating-manager-menu .inner').css('display') == 'block') {
    displaystyle = 'none';
  } else {
    displaystyle = 'block';
  }
  jQuery.get(Drupal.settings.basePath + 'floatingmanagermenu/positioning_set/' + jQuery('#floating-manager-menu').css('top') + '/' + jQuery('#floating-manager-menu').css('left') + '/' + displaystyle);
  jQuery("#floating-manager-menu .inner").slideToggle();
}