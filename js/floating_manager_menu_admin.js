jQuery(document).ready(function() {

  jQuery('input.color').focus(function() {
    jQuery('input.color').parent().removeClass('item-selected');
    jQuery('input.color').css('font-weight', 'normal');
    console.log(jQuery(this).parent());
    jQuery(this).parent().addClass('item-selected');
    jQuery.farbtastic('#colorpicker').linkTo(this);
  });
  
  jQuery('#colorpicker').farbtastic();

  jQuery('input.color').each(
    function() {
      jQuery.farbtastic('#colorpicker').linkTo(this);
    }
  );

  jQuery.farbtastic('#colorpicker').linkTo(null);

});